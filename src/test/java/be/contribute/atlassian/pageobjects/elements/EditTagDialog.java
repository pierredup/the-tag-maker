package be.contribute.atlassian.pageobjects.elements;

import be.contribute.atlassian.pageobjects.page.tag.TagListPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.webdriver.bitbucket.element.Dialog2;
import org.openqa.selenium.By;


public class EditTagDialog extends Dialog2 {

    private final Object[] args;

    @ElementBy(id = "message-input")
    private PageElement descriptionElement;
    @ElementBy(id = "commitId-input")
    private PageElement commitElement;
    private By myLocator;

    public EditTagDialog(By locator, TimeoutType timeoutType) {
        super(locator, timeoutType);
        this.args = new Object[0];
        myLocator = locator;
    }

    public EditTagDialog(By locator, TimeoutType timeoutType,  Object... args) {
        super(locator, timeoutType);
        this.args = args;
        myLocator = locator;
    }

    public void clickConfirm() {
        clickMainAction();
    }

    public void setDescription(String description) {
        descriptionElement.clear();
        descriptionElement.type(description);
    }

    public void setCommitId(String commit) {
        commitElement.clear();
        commitElement.type(commit);
    }


    public TagListPage clickConfirmAndBind() {
        clickConfirm();
        return pageBinder.bind(TagListPage.class, args);
    }

    public EditTagDialog clickConfirmAndExpectErrors() {
        mainActionButton.click();
        return pageBinder.bind(getClass(),myLocator, TimeoutType.AJAX_ACTION, args);
    }

    public String getCommitInputErrorMessage() {
       return commitElement.getAttribute("data-aui-notification-error");
    }
    public String getDescriptionInputErrorMessage() {
       return descriptionElement.getAttribute("data-aui-notification-error");
    }

    public String getDescription() {
        return descriptionElement.getText();
    }

    public String getCommitId() {
        return commitElement.getValue();
    }
}
