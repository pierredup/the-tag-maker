package be.contribute.atlassian.pageobjects.page.tag;


import be.contribute.atlassian.pageobjects.page.CustomCommitPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.webdriver.bitbucket.page.BitbucketPage;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

import static org.fest.assertions.api.Assertions.assertThat;

public class CreateTagPage extends BitbucketPage {

    private static final String SERVLETURL = "/plugins/servlet/create-tag";
    @ElementBy(id = "create-tag-submit")
    private PageElement submitButton;

    @ElementBy(id = "tagName")
    private PageElement tagNameInput;

    @ElementBy(id = "description")
    private PageElement descriptionTextArea;


    private final String projectKey;
    private final String repoSlug;
    private final String csid;
    private final boolean personal;
    private final String lastPage;

    public CreateTagPage(String projectKey, String repoSlug, String csid) {
        this(projectKey, repoSlug, csid, false, null);
    }

    public CreateTagPage(String projectKey, String repoSlug, String csid, boolean personal) {
        this(projectKey, repoSlug, csid, personal, null);

    }
    public CreateTagPage(String projectKey, String repoSlug, String csid, boolean personal, String lastPage) {
        this.projectKey = projectKey;
        this.repoSlug = repoSlug;
        this.csid = csid;
        this.personal = personal;
        this.lastPage = lastPage;
    }

    @Override
    public String getUrl() {
        String path = SERVLETURL + "?projectKey=" + projectKey + "&repoSlug=" + repoSlug + "&commitId=" + csid;
        if (personal) {
            path += "&personal=true";
        }
        if (lastPage !=null) {
            path += "&lastPage=" +lastPage;
        }
        return path;
    }

    public void setDescription(String description) {
        descriptionTextArea.clear();
        descriptionTextArea.type(description);
    }

    public void setTagName(String tagName) {
        tagNameInput.clear();
        tagNameInput.type(tagName);
    }

    public void submit() {
        submitButton.click();
    }

    public CreateTagPage submitIncorrect() {
        submit();
        return pageBinder.bind(this.getClass(), projectKey, repoSlug, csid);
    }

    public CustomCommitPage submitCorrectInput() {
        submit();
        return pageBinder.bind(CustomCommitPage.class, projectKey, repoSlug, csid, personal);
    }

    public <T> T submitCorrectInputAndRedirect(Class<T> pageClass, Object... args) {
        submit();
        return pageBinder.bind(pageClass, args);
    }

    public void assertHasErrors(String... errorMessages) {
        assertThat(getErrorMessages()).containsOnly(errorMessages);
    }

    private ArrayList<String> getErrorMessages() {
        ArrayList<String> messages = new ArrayList<String>();
        List<PageElement> errorElements = elementFinder.findAll(By.className("error"));
        for (PageElement errorElement : errorElements) {
            messages.add(errorElement.getText());
        }
        return messages;
    }
}
