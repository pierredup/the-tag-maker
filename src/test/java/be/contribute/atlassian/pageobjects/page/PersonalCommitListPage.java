package be.contribute.atlassian.pageobjects.page;


import com.atlassian.webdriver.bitbucket.page.CommitListPage;
import com.atlassian.webdriver.bitbucket.util.UrlUtils;
import org.apache.commons.lang.StringUtils;

public class PersonalCommitListPage extends CommitListPage{

    private final String myUntil;

    public PersonalCommitListPage(String projectKey, String repoSlug) {
        this(projectKey, repoSlug, "");
    }

    public PersonalCommitListPage(String projectKey, String repoSlug, String until) {
        super(projectKey, repoSlug, until);
        myUntil = until;
    }

    @Override
    public String getUrl() {
        String encodedAt = UrlUtils.uriEncode(myUntil);
        String queryParams = StringUtils.isNotBlank(myUntil) ? "?until=" + encodedAt : "";
        return "/users/" + projectKey + "/repos/" + slug + "/commits" + queryParams;
    }
}
