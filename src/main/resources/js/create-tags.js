/**
 * Created with IntelliJ IDEA.
 * User: tomas
 * Date: 29/07/14
 * Time: 11:43
 * To change this template use File | Settings | File Templates.
 */
/**
 * Created with IntelliJ IDEA.
 * User: tomas
 * Date: 25/07/14
 * Time: 13:17
 * To change this template use File | Settings | File Templates.
 */
(function ($) {
    window.Contribute = window.Contribute || {};
    Contribute.Changeset = Contribute.Changeset || {};

    function coerceToJson(objecttOrJson) {
        return objecttOrJson.toJSON ? objecttOrJson.toJSON() : objecttOrJson;
    }

    function getCreateTagLinkContext(context) {
        var changeset = context['changeset'];
        console.log(changeset);
        console.log(context.changeset);
        console.log(context);
        var toJson = coerceToJson(changeset);
        console.log(toJson);
        return  toJson;
    }

    Contribute.Changeset.getCreateTagLinkContext = getCreateTagLinkContext;

    $(document).on('click', '.create-tag', function (e) {
        e.preventDefault();
        var link = AJS.contextPath() + "/plugins/servlet/create-tag";
        var repositoryBuilder = require("bitbucket/util/navbuilder");
        var currentRepo = repositoryBuilder.currentRepo();
        var path = currentRepo._path(),
            projectKey = path.components[1].toLowerCase(),
            repoType = path.components[0].toLowerCase(),
            repoSlug = path.components[3].toLowerCase();
        var commitId = AJS.$(this).data("commit-id");
        var extra = "";
        if(repoType=== "users") {
            extra += "&personal=true";
        }
        window.location.replace(link + "?projectKey=" + projectKey + "&repoSlug="+ repoSlug +"&commitId="+commitId + extra);

    });
}(AJS.$));
