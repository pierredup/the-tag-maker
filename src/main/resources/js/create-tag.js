/**
 * Created with IntelliJ IDEA.
 * User: tomas
 * Date: 25/07/14
 * Time: 13:17
 * To change this template use File | Settings | File Templates.
 */
(function ($) {
    window.Contribute = window.Contribute || {};
    Contribute.Commit = Contribute.Commit || {};

    function coerceToJson(objecttOrJson) {
        return objecttOrJson.toJSON ? objecttOrJson.toJSON() : objecttOrJson;
    }

    function getCreateTagLinkContext(context) {
        console.log("Getting Create Tag Link Context");
        var changeset = context['commit'];
        console.log(changeset);
        console.log(context.commit);
        console.log(context);
        var toJson = coerceToJson(changeset);
        console.log(toJson);
        return  toJson;
    }

    Contribute.Commit.getCreateTagLinkContext = getCreateTagLinkContext;

    $(document).on('click', '.create-tag', function (e) {
        e.preventDefault();
        var link = AJS.contextPath() + "/plugins/servlet/create-tag";
        var repositoryBuilder = require("bitbucket/util/navbuilder");
        var currentRepo = repositoryBuilder.currentRepo();
        var path = currentRepo._path(),
            projectKey = path.components[1].toLowerCase(),
            repoType = path.components[0].toLowerCase(),
            repoSlug = path.components[3].toLowerCase();
           var commitId = AJS.$(this).data("commit-id");
        var extra = "";
        if(repoType=== "users") {
            extra += "&personal=true";
        }
        window.location.replace(link + "?projectKey=" + projectKey + "&repoSlug="+ repoSlug +"&commitId="+commitId + extra);

    });
}(AJS.$));
