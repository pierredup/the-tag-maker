define('tag-maker/feature/tag-table', [
    'aui',
    'jquery',
    'lodash',
    'bitbucket/util/navbuilder',
    'bitbucket/util/events',
], function(
    AJS,
    $,
    _,
    nav,
    events
    ) {

    'use strict';

    function TagTable(options) {
    //    PagedTable.call(this, $.extend({}, TagTable.defaults, options));
    }

    TagTable.defaults = {
        filterable: false,
        start:0,
        pageSize: 5, // this must be less than ref.metadata.max.request.count
        noneFoundMessageHtml: AJS.escapeHtml(AJS.I18n.getText('tag-maker.web.no-tags-found.message')),
        noneMatchingMessageHtml: AJS.escapeHtml(AJS.I18n.getText('bitbucket.web.repository.branch.table.no.matches')),
    };

    $.extend(TagTable.prototype);

    TagTable.prototype.buildUrl = function (start, limit) {
        var params = {
            start: start,
            limit: limit
        };  /*

        if (filter) {
            params.filterText = filter;
        }

        return nav.rest().currentRepo()
            .branches()
            .withParams(params)
            .build();    */

        var url = nav.newBuilder()
            .addPathComponents("rest", "tag-maker", "1.0", "tag", "project", pageState.getProject().getKey(), "rep", pageState.getRepository().getSlug(), "tags")
            .withParams(params)
            .buildAbsolute();
        console.log(url);
        return url;
    };


    TagTable.prototype.handleNewRows = function (tagPage, attachmentMethod) {
        console.log(attachmentMethod);
        console.log(tagPage);
        console.log(tagPage.values);
        var permitted = AJS.$('#basic-action').length > 0;
        this.$table.find('tbody')[attachmentMethod](Contribute.Stash.Templates.TagList.tableRows({
            tags: tagPage.values,
            repository: pageState.getRepository().toJSON(),
            permission: permitted
        }));
    };

    return TagTable;

});

jQuery(document).ready(function () {
    var Table = require('tag-maker/feature/tag-table');
    var tagTable = new Table({
        target: '#tag-list'
    });
    tagTable.init();
    console.log(tagTable);
});