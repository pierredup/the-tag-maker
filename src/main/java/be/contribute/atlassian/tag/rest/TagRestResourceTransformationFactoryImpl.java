package be.contribute.atlassian.tag.rest;

import com.atlassian.bitbucket.avatar.AvatarService;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.nav.NavBuilder;
import com.atlassian.bitbucket.repository.RefService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.util.DateFormatter;


public class TagRestResourceTransformationFactoryImpl implements TagRestResourceTransformationFactory {

    private static final int MIN_SIZE = 32;

    private final NavBuilder navBuilder;
    private final CommitService commitService;
    private final AvatarService avatarService;
    private final RefService refService;
    private final DateFormatter dateFormatter;

    public TagRestResourceTransformationFactoryImpl(NavBuilder navBuilder, CommitService commitService, AvatarService avatarService, RefService refService, DateFormatter dateFormatter) {
        this.navBuilder = navBuilder;
        this.commitService = commitService;
        this.avatarService = avatarService;
        this.refService = refService;
        this.dateFormatter = dateFormatter;
    }



    @Override
    public DetailedTagRestTransformFunction getTransformation( Repository repository) {
        DetailedTagRestTransformFunction function = new DetailedTagRestTransformFunction(navBuilder, commitService, avatarService, refService, dateFormatter);
        function.setRepository(repository);
        return function;
    }
}
