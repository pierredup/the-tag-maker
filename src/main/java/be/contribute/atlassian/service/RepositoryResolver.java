package be.contribute.atlassian.service;


import com.atlassian.bitbucket.repository.Repository;

public interface RepositoryResolver {
    Repository resolveRepository(String projectKey, String repoSlug);
    Repository resolveRepository(String projectKey, String repoSlug, boolean personal);
}
