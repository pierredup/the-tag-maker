package be.contribute.atlassian.service;

import be.contribute.atlassian.web.CommitViewable;
import com.atlassian.bitbucket.avatar.AvatarRequest;
import com.atlassian.bitbucket.avatar.AvatarService;
import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.nav.NavBuilder;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.util.DateFormatter;

import java.util.Date;



public class ViewableManagerImpl implements ViewableManager {

    private static final int MIN_SIZE = 32;
    private AvatarService avatarService;
    private CommitService commitService;
    private NavBuilder navBuilder;
    private DateFormatter dateFormatter;

    public ViewableManagerImpl(AvatarService avatarService, CommitService commitService, NavBuilder navBuilder, DateFormatter dateFormatter) {
        this.avatarService = avatarService;
        this.commitService = commitService;
        this.navBuilder = navBuilder;
        this.dateFormatter = dateFormatter;
    }

    @Override
    public CommitViewable createViewAble(Commit commit, Repository repository) {
        CommitViewable commitViewable = new CommitViewable();
        commitViewable.setCommitUrl(getCommitURL(repository, commit.getId()));
        commitViewable.setCommitDate(formatDate(commit.getAuthorTimestamp()));
        commitViewable.setCommitHash(commit.getId());
        commitViewable.setShortCommitHash(commit.getDisplayId());
        commitViewable.setName(commit.getAuthor().getName());
        commitViewable.setAvatarUrl(avatarService.getUrlForPerson(commit.getAuthor(), new AvatarRequest(false, MIN_SIZE)));
        commitViewable.setMessage(commit.getMessage());
        return commitViewable;
    }

    private String formatDate(Date date) {
        return dateFormatter.formatDate(date, DateFormatter.FormatType.LONG);
    }

    private String getCommitURL(Repository repository, String commitHash) {
        return navBuilder.project(repository.getProject()).repo(repository).commit(commitHash).buildAbsolute();
    }
}
