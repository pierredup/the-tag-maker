package be.contribute.atlassian.service;


import be.contribute.atlassian.web.CommitViewable;
import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.repository.Repository;

public interface ViewableManager {
    public CommitViewable createViewAble(Commit changeset, Repository repository);
}
