package be.contribute.atlassian.servlet;

import be.contribute.atlassian.service.RepositoryResolver;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.Map;


public abstract class AbstractTagServlet extends HttpServlet {
    protected final SoyTemplateRenderer soyTemplateRenderer;
    protected final RepositoryService repositoryService;
    protected final RepositoryResolver repositoryResolver;
    protected final ApplicationPropertiesService applicationPropertiesService;



    public AbstractTagServlet(SoyTemplateRenderer soyTemplateRenderer, RepositoryService repositoryService, RepositoryResolver repositoryResolver, ApplicationPropertiesService applicationPropertiesService) {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.repositoryService = repositoryService;
        this.repositoryResolver = repositoryResolver;
        this.applicationPropertiesService = applicationPropertiesService;
    }

    public void doGetTestable(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    public void doPostTestable(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    protected void render(HttpServletResponse resp, String templateName, Map<String, Object> data) throws IOException, ServletException {
        resp.setContentType("text/html;charset=UTF-8");
        try {
            soyTemplateRenderer.render(resp.getWriter(),
                    getResource(),
                    templateName,
                    data);
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            throw new ServletException(e);
        }
    }

    protected URI getBaseUrl() {
        return applicationPropertiesService.getBaseUrl();
    }

    public abstract String getResource() ;
}
