package be.contribute.atlassian.enums;


public enum RefType {
    TAG, COMMIT, NONE
}
