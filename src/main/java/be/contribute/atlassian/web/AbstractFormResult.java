package be.contribute.atlassian.web;

import java.util.HashMap;
import java.util.Map;


public class AbstractFormResult {
    private Map<String,String> errors = new HashMap<String, String>();

    public Map<String, String> getErrors() {
        return errors;
    }

    public void addError(String fieldName, String errorMessage) {
        errors.put(fieldName, errorMessage);
    }

    public boolean hasErrors() {
        return errors.size() > 0;
    }
}
