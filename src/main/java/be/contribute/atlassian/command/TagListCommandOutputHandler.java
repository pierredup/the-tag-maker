package be.contribute.atlassian.command;

import com.atlassian.bitbucket.scm.CommandOutputHandler;
import com.atlassian.utils.process.ProcessException;
import com.atlassian.utils.process.Watchdog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class TagListCommandOutputHandler implements CommandOutputHandler<List<String>> {

    private Watchdog watchdog;
    private List<String> lines = new ArrayList<String>();
    private Logger log = LoggerFactory.getLogger(TagListCommandOutputHandler.class);

    @Nullable
    @Override
    public List<String> getOutput() {
        return lines;
    }

    @Override
    public void process(InputStream inputStream) throws ProcessException {
        try {
            BufferedReader in = getBufferedReader(inputStream);
            String line;
            while ((line = in.readLine()) != null) {
                lines.add(line);
            }
        } catch (UnsupportedEncodingException e) {
            log.error("Unsupported encoding...", e);
        } catch (IOException e) {
           log.error("Encountered an IO Exception: ", e);
        }
    }

    @Override
    public void complete() throws ProcessException {
        //To change body of implemented methods use File | Settings | File Templates.
        log.debug("TagListCommandOutputHandler completed");
    }

    @Override
    public void setWatchdog(Watchdog watchdog) {
        this.watchdog = watchdog;
    }

    protected BufferedReader getBufferedReader(InputStream inputStream) throws UnsupportedEncodingException {
        return new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
    }
}
